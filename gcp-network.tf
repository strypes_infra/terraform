resource "google_compute_network" "swarm" {
  provider = "google.strypes_colab_iac"
  name = "swarm-network"
}

resource "google_compute_firewall" "swarm" {
  provider = "google.strypes_colab_iac"
  name     = "public-services-firewall"
  network  = "${google_compute_network.swarm.name}"

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22", "80", "443", "9000"]
  }

  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "swarm-interconnect" {
  provider = "google.strypes_colab_iac"
  name     = "allow-swarm-interconnect"
  network  = "${google_compute_network.swarm.name}"



  allow {
    protocol = "tcp"
    ports    = ["2377"]
  }

  source_tags = ["swarm-node"]
  target_tags = ["swarm-node"]
}