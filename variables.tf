variable "gcp_colab_credentials" {
  description = "File that contains your service account private key in JSON format"
  default     = "~/.gcloud/terraform_colab.json"
}

variable "region" {
  description = "Location for your resources to be created in"
  default     = "europe-west1"
}

variable "zone" {
  description = "Availability zone"
  default     = "europe-west1-b"
}

variable "ssh_user" {
  description = "GCE SSH service account"
  default     = "strypes"
}

variable "ssh_pub_key_file" {
  description = "SSH Public key path"
  default     = "~/.ssh/id_rsa_ansible.pub"
}

variable "image_name" {
  description = "Image to be used"
  default     = "centos-7"
}

variable "swarm_managers" {
  description = "Number of Swarm managers"
  default     = 1
}

variable "swarm_managers_instance_type" {
  description = "Machine type"

# Micro does not have enough memory to handle even pip install
#  default     = "g1-small"
  default     = "n1-standard-1"
}

variable "swarm_workers" {
  description = "Number of Swarm workers"
  default     = 0
}

variable "swarm_workers_instance_type" {
  description = "Machine type"

# Micro does not have enough memory to handle even pip install
  default     = "g1-small"
}

variable "cloudflare_api_key" {
  description = "Cloudflare secret API key"
}
