# Configure the Cloudflare provider
provider "cloudflare" {
  version    = "~> 2.2"
  alias      = "sicera_org"
  email      = "nasko@sicera.org"
  api_key    = "${var.cloudflare_api_key}"
  account_id = "015fe3f83c75e60f271c4c92520fc7e8"
}

# Create DNS records
resource "cloudflare_record" "dockerprd" {
  provider = "cloudflare.sicera_org"
  zone_id  = "fef6c97faac282f1b983f1891970905b"
  name     = "strypes-democi"
  proxied  = false
  value    = "${google_compute_instance.managers.0.network_interface.0.access_config.0.nat_ip}"
  type     = "A"
  ttl      = 120
}