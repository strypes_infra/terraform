# Preparation

## Permissions for the service account

user@cloudshell:~/projects/terraform (strypes-colab-iac)$ gcloud iam service-accounts keys create ~/.gcloud/terraform_colab.json --iam-account=sa-terraform@strypes-colab-iac.iam.gserviceaccount.com
created key [cd66d8a03d21f3f76c8e12aba78ced0c8c6e7fd0] of type [json] as [/home/user/.gcloud/terraform_colab.json] for [sa-terraform@strypes-colab-iac.iam.gserviceaccount.com]

## Clone the repository

## Test the configuration
terraform init -upgrade  
terraform plan -var-file=$(readlink -fn ~/.cloudflare_api_key.tfvar) -out=/tmp/tf_plan -state=../colab_tfstate/terraform.tfstate  
terraform apply -state=../colab_tfstate/terraform.tfstate "/tmp/tf_plan"  

Do not forget to upload the new tfstate to your snippet when running it manually.