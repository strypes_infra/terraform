provider "google" {
  version     = "~> 2.20"
  alias       = "strypes_colab_iac"
  credentials = "${file("${var.gcp_colab_credentials}")}"
  project     = "strypes-colab-iac"
  region      = "${var.region}"
}

resource "google_compute_project_metadata" "strypes_ansible_ssh_keys" {
  provider = "google.strypes_colab_iac"
  metadata = {
    ssh-keys = <<EOF
${var.ssh_user}:${file(var.ssh_pub_key_file)}
elena_nedelcheva:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC9UGYdTmQCNW96+ik01Ql90+JAiYynrsuLE5ZxzYo9c+f/CFSAQeYn8JSnibrPZTuEMAz/3vnw9sgOUrSnppnYCAIx8bOd6YJDVmymAk8Q1dhXLkCMA85wTmiPkIls/t8G6yOSGdU3MkiVj66xCgREHBmUk2VAMGCKOGv3aAhm1AlAABe/n2ncWq+25R0DlLEfNcf16GvKVwKtrJJj/YTi6Y8iNYQA1SpNvw/pCvdtRYh3OrQ6NlK0Eh0fCEMTBYPD3SDs7Fv1x7645O5ptDbgpfqL8bnbEHyKTkiokwGrL33/Yzy7gZadugbX5lPZp3isGobCnCk4dlC1yixZvUQp
nasko:ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGBpkV7UsshBIA0y9YBSCaCYDG7Rtxt7TOoHTjTfJNWv nasko@Tsonkovi-N551JW
atanas_tsonkov:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDUiBNKTTCBx+0ShjzQQ7c4AVX+5DzI3q6Or5FSUMpclWExrHP9l+RTMdO6RVbN0AeQBpyOaZWoM7MjkWH2bRxR0q8sJW7q3wv8Tz0UEjfs4ptkB3+r0apxAf/OSKxhwtamow0VcfKuMbvVucfMnafNWPMiybhe8JKFr34Acxu3VxSwOOtzh9y1i48FFrJJBZtTtzQtfIovREAOI8EhtWX+tSYpCSm1Tj1q15PGl6PdiC7Hf0h+QIn+H4CwtKm1RjOk9QR5PYsoYwNSRifEhFIigPw5rZh/4+l05GTQvYAdwShE8mO9hsi1yn312ZHlyr1XtTYSIhKW5WIimRXcaHfF
EOF
  }
}
