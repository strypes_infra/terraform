resource "google_compute_instance" "workers" {
  provider     = "google.strypes_colab_iac"
  count        = "${var.swarm_workers}"
  name         = "gcp-worker-${count.index + 1}"
  machine_type = "${var.swarm_workers_instance_type}"
  zone         = "${var.zone}"
  allow_stopping_for_update = true

  depends_on = ["google_compute_instance.managers"]

  boot_disk {
    initialize_params {
      image = "${var.image_name}"
    }
  }

  tags = ["swarm-node"]

  metadata = {
    block-project-ssh-keys = false
  }

  labels = {
    swarm_role = "worker"
  }

  network_interface {
    network       = "${google_compute_network.swarm.name}"
    access_config {
        // Ephemeral IP
    }
  }

}
