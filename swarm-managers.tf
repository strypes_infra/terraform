resource "google_compute_instance" "managers" {
  provider     = "google.strypes_colab_iac"
  count        = "${var.swarm_managers}"
  name         = "gcp-manager-${count.index + 1}"
  machine_type = "${var.swarm_managers_instance_type}"
  zone         = "${var.zone}"
  allow_stopping_for_update = true
  
  boot_disk {
    initialize_params {
      image = "${var.image_name}"
    }
  }

  tags = ["swarm-node"]

  metadata = {
    block-project-ssh-keys = false
  }

  labels = {
    swarm_role = "manager"
  }

  network_interface {
    network       = "${google_compute_network.swarm.name}"
    access_config {
        // Ephemeral IP
    }
  }

}

output "manager_ip" {
    value = "${google_compute_instance.managers.0.network_interface.0.access_config.0.nat_ip}"
}
